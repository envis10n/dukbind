extern crate bindgen;
extern crate cc;

use std::env;
use std::path::PathBuf;
use std::boxed::Box;
use std::collections::HashSet;

// Duplicate types fixed cf. https://github.com/rust-lang/rust-bindgen/issues/687#issuecomment-450750547

#[derive(Debug)]
struct IgnoreMacros(HashSet<String>);

impl bindgen::callbacks::ParseCallbacks for IgnoreMacros {
    fn will_parse_macro(&self, name: &str) -> bindgen::callbacks::MacroParsingBehavior {
        if self.0.contains(name) {
            bindgen::callbacks::MacroParsingBehavior::Ignore
        } else {
            bindgen::callbacks::MacroParsingBehavior::Default
        }
    }
}

fn main() {
    cc::Build::new()
        .file("duktape/src/duktape.c")
        .include("duktape/src")
        .compile("duktape");

    let ignored_macros = IgnoreMacros(
        vec![
            "FP_INFINITE".into(),
            "FP_NAN".into(),
            "FP_NORMAL".into(),
            "FP_SUBNORMAL".into(),
            "FP_ZERO".into(),
            "IPPORT_RESERVED".into(),
        ].into_iter().collect(),
    );
    
    let bindings = bindgen::Builder::default()
        .header("duktape/src/duktape.h")
        .parse_callbacks(Box::new(ignored_macros))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("duktape_bindings.rs"))
        .expect("Couldn't write bindings!");
}
